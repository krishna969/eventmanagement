package com.broadway.eventmanagement.controller;

import com.broadway.eventmanagement.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController  {
    @Autowired
    EventService eventService;

    @GetMapping("/index")
    public String indexPage(Model model)
    {
        model.addAttribute("eventList",eventService.allEvent());
        return "index";
    }

    @GetMapping("/events")
    public String eventPage()
    {
        return "events";
    }
    @GetMapping("/contact")
    public String contactPage()
    {
        return "contact";
    }
    @GetMapping("/speakers")
    public String speakersPage()
    {
        return "speakers";
    }
    @GetMapping("/news")
    public String newsPage()
    {
        return "newspage";
    }





}
